var dailyQuestApp = angular.module('dailyQuestApp', [
    'ngRoute',
    'dailyQuestApp.filters',
    'dailyQuestApp.services',
    'dailyQuestApp.directives',
    'dailyQuestApp.controllers'
]);

dailyQuestApp.filters = angular.module('dailyQuestApp.filters', []);
dailyQuestApp.services = angular.module('dailyQuestApp.services', []);
dailyQuestApp.directives = angular.module('dailyQuestApp.directives', []);
dailyQuestApp.controllers = angular.module('dailyQuestApp.controllers', []);

dailyQuestApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        bodyClass: "index",
        controller: 'indexCtrl',
        templateUrl: 'partials/index.html'
    });
    $routeProvider.when('/index', {
        bodyClass: "index",
        controller: 'indexCtrl',
        templateUrl: 'partials/index.html'
    });
    $routeProvider.when('/test', {
        bodyClass: "test",
        controller: 'testCtrl',
        templateUrl: 'partials/test.html'
    });
}]);


