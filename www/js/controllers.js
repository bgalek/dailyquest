dailyQuestApp.controllers.controller('indexCtrl',
    ['$scope',
        function ($scope) {
            $scope.helloTo = {};
            $scope.helloTo.title = "World, AngularJS";
        }]);

dailyQuestApp.controllers.controller('testCtrl',
    ['$scope',
        function ($scope) {
            navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
                destinationType: Camera.DestinationType.DATA_URL
            });

            function onSuccess(imageData) {
                $scope.image = "data:image/jpeg;base64," + imageData;
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }
        }]);